using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        

        [HttpPost("RegistraVenda")]
        public IActionResult RegistraVenda(Venda venda)
        {
            return Ok(venda);
        }

        [HttpGet("BuscarVenda")]
        public IActionResult BuscarVenda(int id){

            return Ok(id);

        }

        [HttpPut("AtualizarVenda")]
        public IActionResult AtualizarVenda(int id, string status){

            return Ok(id);

        }

    }
}