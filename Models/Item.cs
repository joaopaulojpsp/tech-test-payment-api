using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal Valor { get; set; }
        public int Quantidade { get; set; }

        public Item(int id, string nome, decimal valor, int quantidade){

            Id = id;
            Nome = nome;
            Valor = valor;
            Quantidade = quantidade;

        }

    }
}