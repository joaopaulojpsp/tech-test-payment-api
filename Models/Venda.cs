using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        
        public int Id { get; set; } 
        public Vendedor Vendedor { get; set; }
        public List<Item> Itens { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; }

        public Venda(int id, Vendedor vendedor, List<Item> itens){

            Id = id;
            Vendedor = vendedor;
            Itens = itens;
            Data =  DateTime.Now;
            Status = "Aguardando pagamento";

        }

        public void AtualizarVenda(){


        }


    }
}